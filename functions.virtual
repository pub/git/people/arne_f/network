#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

function virtual_init() {
	module_load 8021q

	ebtables-restore <<EOF
*filter
:INPUT ACCEPT
:FORWARD ACCEPT
:OUTPUT ACCEPT

*broute
:BROUTING ACCEPT
-A BROUTING -p 802_1Q -j DROP
EOF
}

init_register virtual_init

function virtual_create() {
	local port=$(devicify ${1})
	local vid=${2}
	local mac=${3}
	local newport=${port}v${vid}

	if [ -z "${mac}" ]; then
		mac=$(mac_generate)
	fi

	log INFO "Creating virtual device '${newport}' with address '${mac}'."

	local oldport=$(virtual_get_by_parent_and_vid ${port} ${vid})

	if device_exists ${oldport}; then
		local differences

		if [ "${oldport}" != "${newport}" ]; then
			differences="${differences} name"
		fi
		if [ "$(device_get_address ${oldport})" != "${mac}" ]; then
			differences="${differences} address"
		fi
		
		echo "differences: $differences"

		if [ -n "${differences}" ]; then
			if device_is_used ${oldport}; then
				error_log "There was a device '${oldport}' set up with VID '${vid}' and parent '${port}' which is used somewhere else. Cannot go on." 
				return ${EXIT_ERROR}
			else
				log DEBUG "There is a device '${oldport}' but it not used, so we grab it to ourselves."
			fi
		else
			log DEBUG "Device '${newport}' already exists and reflects our configuration. Go on."

			device_set_up ${oldport}
			return ${EXIT_OK}
		fi

	else
		log DEBUG "Virtual device '${newport}' does not exist, yet."

		vconfig set_name_type DEV_PLUS_VID_NO_PAD >/dev/null
		vconfig add ${port} ${vid} >/dev/null
		
		if [ $? -ne ${EXIT_OK} ]; then
			error_log "Could not create virtual device '${newport}'."
			return ${EXIT_ERROR}
		fi

		oldport=$(virtual_get_by_parent_and_vid ${port} ${vid})

	fi

	assert device_exists ${oldport}

	if ! device_exists ${oldport}; then
		error "Could not determine the created virtual device '${newport}'."
		return ${EXIT_ERROR}
	fi

	# The device is expected to be named like ${port}.${vid}
	# and will be renamed to the virtual schema
	device_set_name ${oldport} ${newport}

	if [ $? -ne ${EXIT_OK} ]; then
		error_log "Could not set name of virtual device '${newport}'."
		return ${EXIT_ERROR}
	fi

	assert device_exists ${newport}

	# Setting new mac address
	device_set_address ${newport} ${mac}
	
	if [ $? -ne ${EXIT_OK} ]; then
		error_log "Could not set address '${mac}' to virtual device '${newport}'."
		return ${EXIT_ERROR}
	fi

	# Bring up the new device
	device_set_up ${newport}

	return ${EXIT_OK}
}

function virtual_remove() {
	local device=$(devicify ${1})

	log INFO "Removing virtual device '${device}' with address '$(macify ${device})'."

	device_set_down ${device}

	vconfig rem ${device} >/dev/null

	if [ $? -ne ${EXIT_OK} ]; then
		error_log "Could not remote virtual device '${newport}'."
		return ${EXIT_ERROR}
	fi

	return ${EXIT_OK}
}

function virtual_get_parent() {
	local device=${1}

	local parent=$(grep "^${device}" < /proc/net/vlan/config | awk '{ print $NF }')

	if device_exists ${parent}; then
		echo "${parent}"
		return ${EXIT_OK}
	fi

	return ${EXIT_ERROR}
}

function virtual_get_by_parent_and_vid() {
	local parent=${1}
	local vid=${2}

	assert isset parent
	assert isset vid

	local v_port
	local v_id
	local v_parent

	assert [ -e "/proc/net/vlan/config" ]

	fgrep '|' < /proc/net/vlan/config | tr -d '|' | \
		while read v_port v_id v_parent; do
			if [ "${v_parent}" = "${parent}" ] && [ "${v_id}" = "${vid}" ]; then
				echo "${v_port}"
				return ${EXIT_OK}
			fi
		done

	return ${EXIT_ERROR}
}
