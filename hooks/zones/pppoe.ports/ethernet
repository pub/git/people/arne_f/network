#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

. /lib/network/header-port

function _add() {
	local zone=${1}
	local port=${2}
	shift 2

	assert isset zone
	assert isset port

	if ! port_exists ${port}; then
		error "Port '${port}' does not exist."
		exit ${EXIT_ERROR}
	fi

	touch $(zone_dir ${zone})/ports/${port}

	exit ${EXIT_OK}
}

function _edit() {
	_add $@
}

function _rem() {
	local zone=${1}
	local port=${2}
	shift 2

	assert isset zone
	assert isset port

	if ! listmatch ${port} $(zone_get_ports ${zone}); then
		error "Port '${port}' does not belong to '${zone}'."
		error "Won't remove anything."
		exit ${EXIT_ERROR}
	fi

	warning "Removing port '${port}' from '${zone}' will shutdown the zone."

	# Shut down this zone
	zone_down ${zone}

	rm -f $(zone_dir ${zone})/ports/${port}

	exit ${EXIT_OK}
}

function _up() {
	local zone=${1}
	local port=${2}

	assert isset zone
	assert isset port

	assert zone_exists ${zone}
	assert port_exists ${port}

	port_up ${port}

	exit ${EXIT_OK}
}

function _down() {
	local zone=${1}
	local port=${2}

	assert isset zone
	assert isset port

	assert zone_exists ${zone}
	assert port_exists ${port}

	port_down ${port}

	exit ${EXIT_OK}
}

function _status() {
	local zone=${1}
	local port=${2}

	printf "        %-10s - " "${port}"
	if device_is_up ${port}; then
		echo -ne "${COLOUR_UP}    UP    ${COLOUR_NORMAL}"
	else
		echo -ne "${COLOUR_DOWN}   DOWN   ${COLOUR_NORMAL}"
	fi
	echo

	exit ${EXIT_OK}
}

run $@
