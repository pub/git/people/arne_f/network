#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

. /lib/network/header-zone

HOOK_SETTINGS="HOOK PROTOCOL USER SECRET SERVER TUNNEL_ID"

USER=
SECRET=
SERVER="tic.sixxs.net"
PROTOCOL="tic"
TUNNEL_ID=

function _check() {
	assert isset USER
	assert isset SECRET
	assert isset SERVER
	assert isset PROTOCOL
}

function _parse_cmdline() {
	local value

	while [ $# -gt 0 ]; do
		case "$1" in
			--user=*)
				USER=$(cli_get_val ${1})
				;;
			--secret=*)
				SECRET=$(cli_get_val ${1})
				;;
			--server=*)
				SERVER=$(cli_get_val ${1})
				;;
			--protocol=*)
				PROTOCOL=$(cli_get_val ${1})
				;;
			--tunnel-id=*)
				TUNNEL_ID=$(cli_get_val ${1})
				;;
			*)
				echo "Unknown option: $1" >&2
				exit ${EXIT_ERROR}
				;;
		esac
		shift
	done
}

function _up() {
	local zone=${1}
	shift

	assert isset zone

	zone_config_read ${zone}

	aiccu_start ${zone} \
		--server="${SERVER}" \
		--protocol="${PROTOCOL}" \
		--user="${USER}" \
		--secret="${SECRET}" \
		--tunnel-id="${TUNNEL_ID}"

	exit $?
}

function _down() {
	local zone=${1}
	shift

	aiccu_stop ${zone}

	exit ${EXIT_OK}
}

function _status() {
	local zone=${1}

	assert isset zone

	cli_status_headline ${zone}

	zone_config_read ${zone}

	cli_headline "  Configuration:"
	printf "${DEVICE_PRINT_LINE1}" "User:" "${USER}"
	printf "${DEVICE_PRINT_LINE1}" "Secret:" "<hidden>"
	echo
	printf "${DEVICE_PRINT_LINE1}" "Server:" "${SERVER}"
	printf "${DEVICE_PRINT_LINE1}" "Protocol:" "${PROTOCOL}"
	if isset TUNNEL_ID; then
		echo
		printf "${DEVICE_PRINT_LINE1}" "Tunnel ID:" "${TUNNEL_ID}"
	fi
	echo
	printf "${DEVICE_PRINT_LINE1}" "Use default route?" "$(enabled DEFAULTROUTE && echo "enabled" || echo "disabled")"
	printf "${DEVICE_PRINT_LINE1}" "Use peer DNS?" "$(enabled PEERDNS && echo "enabled" || echo "disabled")"

	# Exit if zone is down
	if ! zone_is_up ${zone}; then
		echo # Empty line
		exit ${EXIT_ERROR}
	fi

	cli_headline "  Protocol information:"
	printf "${DEVICE_PRINT_LINE1}" "MTU:" "$(device_get_mtu ${zone})"
	echo

	exit ${EXIT_OK}
}

run $@
