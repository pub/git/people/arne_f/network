#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

. /lib/network/header-port

HOOK_SETTINGS="COST PRIORITY"

function _check() {
	local i
	for i in COST PRIORITY; do
		if isset ${i}; then
			assert isinteger ${i}
		fi
	done
}

function _add() {
	local zone=${1}
	local port=${2}
	shift 2

	assert isset zone
	assert isset port

	if ! port_exists ${port}; then
		error "Port '${port}' does not exist."
		exit ${EXIT_ERROR}
	fi

	config_read $(zone_dir ${zone})/ports/${port}

	while [ $# -gt 0 ]; do
		case "${1}" in
			--priority=*)
				PRIORITY=${1#--priority=}
				;;
			--cost=*)
				COST=${1#--cost=}
				;;
		esac
		shift
	done

	config_write $(zone_dir ${zone})/ports/${port} ${HOOK_SETTINGS}

	exit ${EXIT_OK}
}

function _edit() {
	_add $@
}

function _rem() {
	local zone=${1}
	local port=${2}

	assert isset zone
	assert isset port

	assert zone_exists ${zone}

	if ! listmatch ${port} $(zone_get_ports ${zone}); then
		error "Port '${port}' does not belong to '${zone}'."
		error "Won't remove anything."
		exit ${EXIT_ERROR}
	fi

	if port_exists ${port}; then
		( _down ${zone} ${port} )
	fi

	rm -f $(zone_dir ${zone})/ports/${port}

	exit ${EXIT_OK}
}

function _up() {
	local zone=${1}
	local port=${2}

	assert isset zone
	assert isset port

	assert zone_exists ${zone}
	assert port_exists ${port}

	port_up ${port}

	# Set same MTU to device that the bridge has got
	device_set_mtu ${port} $(device_get_mtu ${zone})

	bridge_attach_device ${zone} ${port}

	# XXX must set cost and prio here

	exit ${EXIT_OK}
}

function _down() {
	local zone=${1}
	local port=${2}

	assert isset zone
	assert isset port

	assert zone_exists ${zone}
	assert port_exists ${port}

	bridge_detach_device ${zone} ${port}

	port_down ${port}

	exit ${EXIT_OK}
}

function _status() {
	local zone=${1}
	local port=${2}

	printf "        %-10s - " "${port}"
	if ! device_is_up ${port}; then
		echo -ne "${COLOUR_DOWN}   DOWN   ${COLOUR_NORMAL}"
	else
		local state=$(stp_port_get_state ${zone} ${port})
		local colour="COLOUR_STP_${state}"
		printf "${!colour}%10s${COLOUR_NORMAL}" ${state}

		echo -n " - DSR: $(stp_port_get_designated_root ${zone} ${port})"
		echo -n " - Cost: $(stp_port_get_cost ${zone} ${port})"
	fi

	echo

	exit ${EXIT_OK}
}

run $@
