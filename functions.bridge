#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

function bridge_attach_device() {
	local bridge=${1}
	local device=${2}

	assert isset bridge
	assert isset device

	assert device_exists ${bridge}
	assert device_exists ${device}

	# If device is already attached, exit silently
	if listmatch ${device} $(bridge_get_members ${bridge}); then
		return ${EXIT_OK}
	fi

	log INFO "Attaching device '${device}' to bridge '${bridge}'."

	brctl addif ${bridge} ${device}
}

function bridge_detach_device() {
	local bridge=${1}
	local device=${2}

	assert isset bridge
	assert isset device
	
	if ! device_exists ${bridge}; then
		error "Bridge '${bridge}' does not exist."
		return ${EXIT_ERROR}
	fi

	if ! device_exists ${device}; then
		return ${EXIT_OK}
	fi

	# If device is not attached, exit silently
	if ! listmatch ${device} $(bridge_get_members ${bridge}); then
		return ${EXIT_OK}
	fi

	log INFO "Detaching device '${device}' from bridge '${bridge}'."

	brctl delif ${bridge} ${device}
}

function bridge_get_members() {
	local bridge=${1}

	assert isset bridge

	local member
	for member in ${SYS_CLASS_NET}/${bridge}/brif/*; do
		member=$(basename ${member})
		if device_exists ${member}; then
			echo "${member}"
		fi
	done
}

function bridge_is_forwarding() {
	local seconds=45
	local zone=${1}

	bridge_has_carrier ${zone} || return ${EXIT_ERROR}

	local device
	while [ ${seconds} -gt 0 ]; do
		for device in ${SYS_CLASS_NET}/${zone}/brif/*; do
			[ -e "${device}/state" ] || continue
			if [ "$(<${device}/state)" = "3" ]; then
				return ${EXIT_OK}
			fi
		done
		sleep 1
		seconds=$((${seconds} - 1))
	done

	return ${EXIT_ERROR}
}

function bridge_has_carrier() {
	local zone=${1}

	local has_carrier=${EXIT_ERROR}

	local device
	for device in ${SYS_CLASS_NET}/${zone}/brif/*; do
		device=$(basename ${device})
		device_exists ${device} || continue

		device_has_carrier ${device} && has_carrier=${EXIT_OK}
	done

	return ${has_carrier}
}
