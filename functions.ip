#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

# A list of supported versions of the IP protocol
IP_SUPPORTED_PROTOCOLS=""

function ip_split_prefix() {
	local address=${1}

	assert isset address

	echo "${address%%/*}"
}

function ip_get_prefix() {
	local address=${1}

	assert isset address

	# Break if no prefix is provided
	[[ ${address} =~ \/ ]] || return ${EXIT_OK}

	echo "${address##*/}"
}

function ip_detect_protocol() {
	local address=${1}

	assert isset address

	local protocol
	for protocol in ${IP_SUPPORTED_PROTOCOLS}; do
		if ${protocol}_is_valid ${address}; then
			log DEBUG "Address '${address}' was detected to be protocol '${protocol}'."
			echo "${protocol}"
			return ${EXIT_OK}
		fi
	done

	log DEBUG "Protocol version of address '${address}' could not be detected."

	return ${EXIT_ERROR}
}

function ip_protocol_is_supported() {
	local proto=${1}

	assert isset proto

	listmatch ${proto} ${IP_SUPPORTED_PROTOCOLS}
}

function ip_address_add() {
	local device=${1}
	local address=${2}

	assert isset address
	assert device_exists ${device}

	local prefix=$(ip_get_prefix ${address})
	address=$(ip_split_prefix ${address})

	assert isset prefix

	# Detect the protocol version
	local protocol=$(ip_detect_protocol ${address}/${prefix})
	assert ip_protocol_is_supported ${protocol}

	case "${protocol}" in
		ipv4)
			if ipv4_detect_duplicate ${device} ${address}; then
				error_log "Duplicate address detected on zone '${device}' (${address})."
				error_log "Cannot continue."
				return ${EXIT_ERROR}
			fi
			;;
	esac

	if ! device_has_ip ${device} ${address}/${prefix}; then
		assert ip addr add ${address}/${prefix} dev ${device}

		log DEBUG "IP address '${address}' (${protocol}) was successfully configured on device '${device}'."

		case "${protocol}" in
			ipv4)
				# Announce our new address to the neighbours
				ipv4_update_neighbours ${device} ${address}
				;;
		esac
	else
		log DEBUG "IP address '${address}' (${protocol}) was already configured on device '${device}'."
	fi

	return ${EXIT_OK}
}

function ip_address_del() {
	local device=${1}
	local address=${2}

	assert isset address
	assert device_exists ${device}

	local prefix=$(ip_get_prefix ${address})
	address=$(ip_split_prefix ${address})

	assert isset prefix

	# Detect the protocol version
	local protocol=$(ip_detect_protocol ${address}/${prefix})
	assert ip_protocol_is_supported ${protocol}

	if device_has_ip ${device} ${address}/${prefix}; then
		assert ip addr del ${address}/${prefix} dev ${device}

		log DEBUG "IP address '${address}' (${protocol}) was successfully removed from device '${device}'."
	else
		log DEBUG "IP address '${address}' (${protocol}) was not configured on device '${device}'."
	fi

	return ${EXIT_OK}
}
