#!/bin/bash
###############################################################################
#                                                                             #
# IPFire.org - A linux based firewall                                         #
# Copyright (C) 2010  Michael Tremer & Christian Schmidt                      #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

function bonding_init() {
	if ! grep -q "^bonding" /proc/modules; then
		modprobe bonding

		bonding_remove bond0
	fi
}

init_register bonding_init

function bonding_create() {
	local device=${1}
	local mac=${2}

	[ -z "${mac}" ] && mac=$(mac_generate)

	log INFO "Creating bonding device '${device}' (${mac})."

	echo "+${device}" > /sys/class/net/bonding_masters
	device_set_address ${device} ${mac}
	device_set_up ${device}
}

function bonding_remove() {
	local device=$(devicify ${1})

	assert isset device

	log INFO "Remove bonding device '${device}'."

	device_set_down ${device}
	echo "-${device}" > /sys/class/net/bonding_masters
}

function bonding_set_mode() {
	local device=${1}
	local mode=${2}

	log INFO "Setting bonding mode on '${device}' '${mode}'."

	echo "${mode}" > /sys/class/net/${device}/bonding/mode
}

function bonding_get_mode() {
	local device=${1}

	local mode mode_num
	read mode mode_num < ${SYS_CLASS_NET}/${device}/bonding/mode
	echo "${mode}"
}

function bonding_enslave_device() {
	local device=$(devicify ${1})
	local slave=$(devicify ${2})
	shift 2

	assert isset device
	assert isset slave

	log INFO "Enslaving slave '${slave}' to '${device}'."

	device_set_down ${slave}
	echo "+${slave}" > /sys/class/net/${device}/bonding/slaves
}

function bonding_get_slaves() {
	local device=${1}

	cat ${SYS_CLASS_NET}/${device}/bonding/slaves
}

function bonding_get_active_slave() {
	local device=${1}

	cat ${SYS_CLASS_NET}/${device}/bonding/active_slave
}

# XXX function bonding_get_lacp_rate?

function bonding_get_miimon() {
	local device=${1}

	cat ${SYS_CLASS_NET}/${device}/bonding/miimon
}

function bonding_set_miimon() {
	local device=${1}
	local miimon=${2}

	echo "${miimon}" > ${SYS_CLASS_NET}/${device}/bonding/miimon
}

function bonding_device_print() {
	local device=${1}

	ethernet_device_print ${device}

	echo # Empty line

	printf "${DEVICE_PRINT_LINE1}" "Mode:" "$(bonding_get_mode ${device})"
	printf "${DEVICE_PRINT_LINE1}" "Slaves:" "$(bonding_get_slaves ${device})"
}

function bonding_slave_get_master() {
	local slave=${1}

	assert isset slave
	assert device_is_bonded ${slave}

	local device
	for device in $(devices_get_all); do
		if device_is_bonding ${device} && listmatch ${slave} $(bonding_get_slaves ${device}); then
			echo "${device}"
			return ${EXIT_OK}
		fi
	done

	return ${EXIT_ERROR}
}
